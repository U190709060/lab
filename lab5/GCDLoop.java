public class GCDLoop {

    public static void main(String[] args) {

        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);

        int greater = a > b ? a : b;
        int smaller = a < b ? a : b;


        int q1, r1;


        do {
            System.out.println("greater = " + greater);
            System.out.println("smaller = " + smaller);
            q1 = greater / smaller;
            r1 = greater % smaller;
            greater = smaller;
            smaller = r1;
        } while (r1 != 0);

        System.out.println("GCD = " + greater);


    }
}